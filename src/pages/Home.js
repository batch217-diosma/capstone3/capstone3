import Banner from "../components/Banner";
import Highlights from "../components/Highlights";

export default function Home(){
	const data = {
		title: "Company Name",
		content: "Company Tag Line",
		destination: "/products",
		label: "Shop now!"
	}

	return(
		<>
			<Banner bannerProp={data}/>
			<Highlights />
		</>
	)
}