import { useEffect, useState } from "react";
 import ProductCard from "../components/ProductCard";


 export default function RetrieveProducts() {


 	const [products, setProducts] = useState([]); 

 	useEffect(() =>{
 		fetch(`${process.env.REACT_APP_ECOM_URL}/products/allproducts`)
 		.then(res => res.json())
 		.then(data => {
 			console.log(data);
 			setProducts(data.map(product =>{
 				return(
 					<ProductCard key={product._id} productProp={product}/>
 				);
 			}));
 		})
 	}, []);
 	return(
 		<>
			<h1 className="text-light m-3 p-3">Products</h1>
			{products}
		</>
	)
}
