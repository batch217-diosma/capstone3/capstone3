import { useState, useEffect, useContext } from 'react';
import { Navigate } from "react-router-dom";
import { Form, Button, Card, Row, Col, Nav } from 'react-bootstrap';
import Swal from "sweetalert2";
import UserContext from "../UserContext";
export default function Login() {

const { user, setUser } = useContext(UserContext);
const [email, setEmail] = useState('');
const [password, setPassword] = useState('');
const [willRedirect, setWillRedirect] = useState(false);


const [isActive, setIsActive] = useState(false);


useEffect(() => {

    if(email !== '' && password !== ''){
        setIsActive(true);
    }else{
        setIsActive(false);
    }

}, [email, password]);

function authenticate(e) {

    e.preventDefault();

    fetch(`${process.env.REACT_APP_ECOM_URL}/users/login`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            email: email,
            password: password
        })
    })
    .then(res => res.json())
    .then(data => {
        console.log(data.access);
        if(data.access !== undefined){
            localStorage.setItem("token", data.access);
            retrieveUserDetails(data.access);

            Swal.fire({
                title: "Login Successful",
                icon: "success",
                text: "Welcome to Zuitt!"
            });
        }
        else{
            Swal.fire({
                title: "Authentication Failed!",
                icon: "error",
                text: "Check your login details and try again."
            });
        }
    });

    setEmail('');
    setPassword('');

}

const retrieveUserDetails = (token) => {


    fetch(`${process.env.REACT_APP_ECOM_URL}/users/details`, {
        method: "POST",
        headers:{
            Authorization: `Bearer ${token}`
        }
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);

        setUser({
            id: data._id,
            isAdmin: data.isAdmin
        });
        if (data.isAdmin === true) {
                setWillRedirect(true);
            } 
    })
}

return (
    willRedirect === true
     ?
    (user.isAdmin !== null)
    ?
        <Navigate to="/" />
   
    :
        <Navigate to="/adminPage" />
    :
    <>
    <Row className="justify-content-center">
        <Col xs md ="6">
         <h1 className="my-5 text-center text-light">Login</h1>
        <Card>
            <Form onSubmit={(e) => authenticate(e)}>
            <Card.Body>
            <Form.Group controlId="userEmail" className="mb-3">
                <Form.Label className="">Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password" className="mb-3">
                <Form.Label className="">Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>
            </Card.Body>
            <Card.Footer>
            <div className="d-grid gap-2">

            {
            isActive
            ?
                <Button variant="primary" type="submit" id="submitBtn" block >
                    Login
                </Button>
            :
                <Button variant="danger" type="submit" id="submitBtn"  block disabled  >
                    Login
                </Button>
            }
            </div>
            </Card.Footer>
            
        </Form>
        
        </Card>
        <p className="text-center mt-3 text-light">
                        Don't have an account yet? <Nav.Link href="/register">Click Here!</Nav.Link> to register.
                    </p>
                </Col> 
                <p>
                   .
                </p>  
                <p>
                   .
                </p>    
                 <p>
                   .
                </p>  
                <p>
                   .
                </p>    
                 <p>
                   .
                </p>  
                <p>
                   .
                </p>    
                 <p>
                   .
                </p>  
                <p>
                   .
                </p>    
                 <p>
                   .
                </p>  
                <p>
                   .
                </p>    
                 <p>
                   .
                </p>  
                <p>
                   .
                </p>           
            </Row>
    </>
)
}
