



import { useState, useEffect, useContext } from "react";
import { useNavigate } from "react-router-dom";
import {Form, Button, Card, Row, Col, Nav} from "react-bootstrap";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function UpdateProduct(){
    
    const { user } = useContext(UserContext);

    const navigate = useNavigate();
	const [name, setName] = useState('');
	const [category, setCategory] = useState('');
	const [brand, setBrand] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [isActive, setIsActive] = useState(false);
	const [productId, setProductId] = useState("");

	const openEdit = (id) => {
        setProductId(id);

        fetch(`${process.env.REACT_APP_ECOM_URL}/products/search/${id}`)
            .then(res => res.json())
            .then(data => {

                console.log(data);

                setName(data.productName);
                setDescription(data.description);
                setPrice(data.price);
                //setStocks(data.stocks);
                //setImgSource(data.imgSource);
            });
        }

    

    const closeEdit = () => {

         console.log(name);
    console.log(category);
    console.log(brand);
    console.log(description);
    console.log(price);
    
    };

   

    useEffect(() =>{


        if((name !== '' && category !=='' && brand !=='' && description !== '' && price !== '' )){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }

    }, [name,category,brand,description,price ])




    function updateProduct (e) {
        e.preventDefault();

      

                fetch(`${process.env.REACT_APP_ECOM_URL}/products/${productId}/update`,{
                    method: "PUT",
                    headers:{
                        "Content-Type": "application/json",
                        "Authorization": `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify({
                        name: name,
                        description: description,
                        category: category,
                        brand: brand,
                        price: price
                        
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);

                    if(data){
                        Swal.fire({
                            title: "Updated Product Successfully!",
                            icon: "success",
                            text: "Updated Product added to the list"
                        });
                        //fetchData();
                     setName('');
                setCategory('');
                setBrand('');
                setDescription('');
                setPrice('');                        
                       // navigate("/login");
                    }
                    else{

                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please try again."
                        });
                        

                    }
                })
               


            }
        
    

    return(

        <>
        <Row className="justify-content-center">
        <Col xs md ="6">
            <h1 className="my-5 text-center text-light">Update Product</h1>
            <Card>
            <Form onSubmit={e => updateProduct(e)}>
            <Card.Body>

            <Form.Group className="mb-3" controlId="ProductName">
                <Form.Label>Product Name</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter product name"
                    value={name}
                    onChange={e => setName(e.target.value)}
                    
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="description">
                <Form.Label>Description</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter description"
                    value={description}
                    onChange={e => setDescription(e.target.value)}
                    
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="category">
                <Form.Label>Category</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter category"
                    value={category}
                    onChange={e => setCategory(e.target.value)}
                    
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="brand">
                <Form.Label>Brand</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter brand name"
                    onChange={e => setBrand (e.target.value)}
                    value={brand}
                    
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="price">
                <Form.Label>Price</Form.Label>
                <Form.Control
                    type="number"
                    placeholder="Product Price"
                    value={price}
                    onChange={e => setPrice(e.target.value)}
                    
                />
            </Form.Group>
            </Card.Body>
            <Card.Footer>
            <div className = "d-grid gap-2">
            {
                isActive
                ?
                    <Button variant="primary" type="submit" id="submitBtn">
                    Update Product
                    </Button>
                :
                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Update Product
                    </Button>
            }
            </div>
            </Card.Footer>
            
            </Form>
             </Card>
                </Col>              
            </Row>
        </>
    )
}