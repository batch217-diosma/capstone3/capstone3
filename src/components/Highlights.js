

import {Col, Row, Card, Button} from "react-bootstrap"
import { Link } from "react-router-dom";
import React from 'react'

export default function Highlights(){
    return(
        <Row className="my-3">
        <h1 className="mb-3 p-3 text-center text-light" >Featured Products</h1>
            <Col xs={12} md={4} className ="mb-3">
                <Card className="cardHighlight p-3 mb-3 ">
                <Card.Img variant="top" src="https://i.ytimg.com/vi/fmSmcSfC5fk/maxresdefault.jpg" className="img"/>
                    <Card.Body>
                        <Card.Title>
                            <h2>Product Name</h2>
                        </Card.Title>
                        <Card.Text>
                            Item Description
                        </Card.Text>
                        <Button as = {Link} to="/products/productId" variant="primary" >Buy Now!</Button>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4} className ="mb-3">
                <Card className="cardHighlight p-3">
                <Card.Img variant="top" src="https://cf.shopee.ph/file/8fdaf814e4fbf8843be7709b93929fc9"/>
                    <Card.Body>
                        <Card.Title>
                            <h2>Product Name</h2>
                        </Card.Title>
                        <Card.Text>
                            Item Description
                        </Card.Text>
                        <Button as = {Link} to="/products/productId" variant="primary" >Buy Now!</Button>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4} className ="mb-3">
                <Card className="cardHighlight p-3 ">
                <Card.Img variant="top" src="https://i.ytimg.com/vi/fmSmcSfC5fk/maxresdefault.jpg" className="rounded-circle img" />
                    <Card.Body>
                        <Card.Title>
                            <h2>Product Name</h2>
                        </Card.Title>
                        <Card.Text>
                           Item Description
                        </Card.Text>
                        <Button as = {Link} to="/products/productId" variant="primary" >Buy Now!</Button>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4} className ="mb-3">
                <Card className="cardHighlight p-3">
                <Card.Img variant="top" src="https://img.freepik.com/free-vector/cute-corgi-dog-sitting-cartoon-vector-icon-illustration-animal-nature-icon-concept-isolated-premium-vector-flat-cartoon-style_138676-4181.jpg?w=740&t=st=1668591084~exp=1668591684~hmac=333cd3a33419c17e5a571209926270a34ba2544fc62bdfd39adf789855d7b71c" />
                    <Card.Body>
                        <Card.Title>
                            <h2>Product Name</h2>
                        </Card.Title>
                        <Card.Text>
                           Item Description
                        </Card.Text>
                        <Button as = {Link} to="/products/productId" variant="primary" >Buy Now!</Button>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4} className ="mb-3">
                <Card className="cardHighlight p-3">
                <Card.Img variant="top" src="https://img.freepik.com/free-vector/cute-corgi-dog-sitting-cartoon-vector-icon-illustration-animal-nature-icon-concept-isolated-premium-vector-flat-cartoon-style_138676-4181.jpg?w=740&t=st=1668591084~exp=1668591684~hmac=333cd3a33419c17e5a571209926270a34ba2544fc62bdfd39adf789855d7b71c" />
                    <Card.Body>
                        <Card.Title>
                            <h2>Product Name</h2>
                        </Card.Title>
                        <Card.Text>
                           Item Description
                        </Card.Text>
                        <Button as = {Link} to="/products/productId" variant="primary" >Buy Now!</Button>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4} className ="mb-3">
                <Card className="cardHighlight p-3">
                <Card.Img variant="top" src="https://img.freepik.com/free-vector/cute-corgi-dog-sitting-cartoon-vector-icon-illustration-animal-nature-icon-concept-isolated-premium-vector-flat-cartoon-style_138676-4181.jpg?w=740&t=st=1668591084~exp=1668591684~hmac=333cd3a33419c17e5a571209926270a34ba2544fc62bdfd39adf789855d7b71c" className="rounded-circle"/>
                    <Card.Body>
                        <Card.Title>
                            <h2>Product Name</h2>
                        </Card.Title>
                        <Card.Text>
                           Item Description
                        </Card.Text>
                        <Button as = {Link} to="/products/productId" variant="primary" >Buy Now!</Button>
                    </Card.Body>
                </Card>
            </Col>
            <p>.</p>
             <p>.</p>
        </Row>
    )
}