

import { useContext } from "react"
import { NavLink } from "react-router-dom";
import { Container, Nav, Navbar } from "react-bootstrap";
import UserContext from "../UserContext";

export default function AppNavbar(){

    const { user } = useContext(UserContext);
	return(

		<Navbar bg="primary" expand="lg" sticky="top">
        <Container fluid>
            <Navbar.Brand as={ NavLink } to="/">Zuitt</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto">
                <Nav.Link as={ NavLink } to="/" end>Home</Nav.Link>
                <Nav.Link as={ NavLink } to="/admin" end>Admin Page</Nav.Link>
                <Nav.Link as={ NavLink } to="/products" end>Products</Nav.Link>
                <Nav.Link as={ NavLink } to="/addproduct" end>Create Product</Nav.Link>

                {
                    
                    (user.id !== null)
                    ?
                        <Nav.Link as={ NavLink } to="/logout" end>Logout</Nav.Link>
                    :
                    <>
                        <Nav.Link as={ NavLink } to="/login" end>Login</Nav.Link>
                        <Nav.Link as={ NavLink } to="/register" end>Register</Nav.Link>
                   
                    </>
                }
               
            </Nav>
            </Navbar.Collapse>
        </Container>
        </Navbar>
    )
}



		