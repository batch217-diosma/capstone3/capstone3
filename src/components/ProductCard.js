

import { Card, Button } from 'react-bootstrap';
import { Link } from "react-router-dom";
export default function ProductCard({productProp}) {


	const { _id, name, description, category, brand, price, stock } = productProp;


    return (
        <Card className="my-3 ">
            <Card.Body>
                <Card.Title>{name}</Card.Title>

                <Card.Subtitle>Description:</Card.Subtitle>

                <Card.Text>{description}</Card.Text>

                 <Card.Subtitle>Category:</Card.Subtitle>

                <Card.Text>{category}</Card.Text>

                 <Card.Subtitle>Brand:</Card.Subtitle>

                <Card.Text>{brand}</Card.Text>

                <Card.Subtitle>Price:</Card.Subtitle>

                <Card.Text>Php {price}</Card.Text>

                <Card.Subtitle>Stock:</Card.Subtitle>

                <Card.Text>{stock} available</Card.Text>

                <Button as={Link} to={`/products/${_id}`} variant="primary">Details</Button>
            </Card.Body>
        </Card>
    )
}